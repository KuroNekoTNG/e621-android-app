﻿using Android.App;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;

using e621AndroidApp.Activities;

using SupportActionDrawerToggle = Android.Support.V7.App.ActionBarDrawerToggle;

namespace e621AndroidApp {
	class ActionBarNavigationToggle : SupportActionDrawerToggle {

		private Activity hostAct;
		private int openedResource, closedResource;

		public ActionBarNavigationToggle( Activity activity, DrawerLayout drawerLayout, int openedResource, int closedResource ) : base( activity, drawerLayout, openedResource, closedResource ) {
			hostAct = activity; // Assigns the host activity to activity.
			this.openedResource = openedResource; // Might get rid of.
			this.closedResource = closedResource; // Might get rid of.
		}

		public override bool OnOptionsItemSelected( IMenuItem item ) {
			Toast.MakeText( hostAct, "Recieved selection!", ToastLength.Short );

			return base.OnOptionsItemSelected( item );
		}

		public override void OnDrawerOpened( View drawerView ) {
			if ( ( string )drawerView.Tag == MainActivity.NAVIGATION_DRAWER ) { // Checks to see if the drawer view is the navigation drawer.
				base.OnDrawerOpened( drawerView );
			}
		}

		public override void OnDrawerClosed( View drawerView ) {
			if ( ( string )drawerView.Tag == MainActivity.NAVIGATION_DRAWER ) { // Checks to see if the drawer view is the navigation drawer.
				base.OnDrawerClosed( drawerView );
			}
		}

		public override void OnDrawerSlide( View drawerView, float slideOffset ) {
			base.OnDrawerSlide( drawerView, slideOffset );
		}
	}
}