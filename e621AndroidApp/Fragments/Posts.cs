﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using Android.Widget;

using e621AndroidApp.E621;

using Newtonsoft.Json;

using Exception = System.Exception;
using Object = Java.Lang.Object;

namespace e621AndroidApp.Fragments {
	public class Posts : Fragment {

		private const string TAG = "e621PostGrid";

		private class PostGridAdapterView : BaseAdapter {
			public override int Count {
				get => posts.Count;
			}

			private IList<Post> posts;
			private Context context;

			public PostGridAdapterView( IList<Post> posts, Context context ) {
				this.posts = posts;
				this.context = context;
			}

			public override Object GetItem( int position ) {
				return null;
			}

			public override long GetItemId( int position ) {
				return posts[position].PostID;
			}

			public override View GetView( int position, View convertView, ViewGroup parent ) {
				View view;
				MemoryStream imageStream;
				ImageButton imgButton;
				LayoutInflater inflater = context.GetSystemService( Context.LayoutInflaterService ) as LayoutInflater;

				if ( convertView is null ) {
					view = inflater.Inflate( Resource.Layout.post_view, parent, false );
				} else {
					view = convertView;
				}

				imgButton = view.FindViewById<ImageButton>( Resource.Id.post_image_button );

				

				imageStream = posts[position].GetPreviewImageStream();

				imgButton.SetImageBitmap( BitmapFactory.DecodeStream( imageStream ) );

				return view;
			}
		}

		public override void OnCreate( Bundle savedInstanceState ) {
			base.OnCreate( savedInstanceState );

			// Create your fragment here
		}

		public override View OnCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
			string json;
			dynamic[] dPosts;
			List<Post> posts = new List<Post>();
			GridView gView;
			View mainView = inflater.Inflate( Resource.Layout.posts, container, false );

			gView = mainView.FindViewById<GridView>( Resource.Id.posts_grid );

			using ( WebClient client = new WebClient() ) {
				client.Headers.Add( HttpRequestHeader.UserAgent, "Kuroneko's Unoffical e621 Android App" );

				json = client.DownloadString( "http://e621.net/post/index.json?tags=rating:s&limit=320" );
			}

			dPosts = JsonConvert.DeserializeObject<dynamic[]>( json );

			foreach ( dynamic post in dPosts ) {
				Post tmpPost;

				try {
					tmpPost = new Post( post );
				} catch ( Exception e ) {
					Log.Wtf( TAG, $"Message: {e.Message}\nSource: {e.Source}\nHelp Link: {e.HelpLink}\nStacktrace: {e.StackTrace}" );
					continue;
				}

				if ( tmpPost.FileExtension.Equals( "swf", StringComparison.CurrentCultureIgnoreCase ) || tmpPost.FileExtension.Equals( "webm", StringComparison.CurrentCultureIgnoreCase ) || tmpPost.FileExtension.Equals( "gif", StringComparison.CurrentCultureIgnoreCase ) ) {
					continue;
				}

				posts.Add( tmpPost );

				Log.Info( "e621PostGrid", $"Successfully converted a dynamic post into a struct. Post ID was: {post.id}" );
			}

			Log.Info( TAG, $"Successfully converted {posts.Count}/{dPosts.Length}, that is a success rate of {( ( ( double )posts.Count ) / dPosts.Length ).ToString( "##0.00%" )}." );

			gView.Adapter = new PostGridAdapterView( posts, Context );

			return mainView;
		}
	}
}