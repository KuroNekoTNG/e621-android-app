﻿using System;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Net;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;

using AlertDialog = Android.Support.V7.App.AlertDialog; // To seperate the AlertDialog in Support from the other one.
using Toolbar = Android.Support.V7.Widget.Toolbar; // To seperate the Toolbar in Support from the other one.
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
using e621AndroidApp.Fragments;

namespace e621AndroidApp.Activities {

	[Activity( Label = "@string/app_name", Theme = "@style/e621BaseTheme", MainLauncher = true )]
	public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener {

		/// <summary>
		/// Used to identify the navigation drawer.
		/// </summary>
		public const string NAVIGATION_DRAWER = "Left";

		private Toolbar toolbar; // Holds a reference to the toolbar.
		private DrawerLayout drawerLayout; // Holds a reference to the drawer layout.
		private NavigationView navDrawer; // Holds a reference to the navigation drawer.
		private AlertDialog meteredDetectedAlert;

		private ActionBarNavigationToggle drawerToggle; // Holds a regerence to the toggle.

		public override bool OnOptionsItemSelected( IMenuItem item ) {
			drawerToggle.OnOptionsItemSelected( item ); // Calls the drawer toggle as to which option is selected.
			return base.OnOptionsItemSelected( item );
		}

		public override void OnConfigurationChanged( Configuration newConfig ) {
			drawerToggle.OnConfigurationChanged( newConfig );
			base.OnConfigurationChanged( newConfig );
		}

		public bool OnNavigationItemSelected( IMenuItem menuItem ) {
			string toastMessage;

			switch ( menuItem.ItemId ) { // Switches on the menu item id.
				case Resource.Id.following_menu_item: // If the item id was of the post menu item.
					toastMessage = "Following item selected.";
					break;
				case Resource.Id.post_menu_item: // If the item id was of the post menu item.
					toastMessage = "Post item selected.";
					break;
				case Resource.Id.popular_menu_item: // If the item id was of the popular menu item.
					toastMessage = "Popular was selected.";
					break;
				case Resource.Id.random_menu_item: // If the item id was of the random menu item.
					toastMessage = "Random was selected!";
					break;
				case Resource.Id.settings_menu_item: // If the item id was of the random menu item.
					toastMessage = "Settings was selected!";
					break;
				default: // This should NEVER trip.
					Log.Wtf( "e621.Nav.Unknown Item", "Unkown menu item." );
					throw new Exception( "Unkown menu item." );
			}

			Toast.MakeText( this, toastMessage, ToastLength.Short ).Show();

			return true;
		}

		// REMINDER: Use this if need be.
		protected override void OnSaveInstanceState( Bundle outState ) {
			base.OnSaveInstanceState( outState );
		}

		// REMINDER: Use this if need be.
		protected override void OnPostCreate( Bundle savedInstanceState ) {
			base.OnPostCreate( savedInstanceState );
		}

		// Creates the activity.
		protected override void OnCreate( Bundle savedInstanceState ) {
			base.OnCreate( savedInstanceState );

			// Set our view from the "main" layout resource
			SetContentView( Resource.Layout.activity_main );

			toolbar = FindViewById<Toolbar>( Resource.Id.toolbar ); // Grabs the toolbar.
			drawerLayout = FindViewById<DrawerLayout>( Resource.Id.drawer_layout ); // Grabs the drawer layout.
			navDrawer = FindViewById<NavigationView>( Resource.Id.nav_view ); // Grabs the ListView that will be on the left, and is used to navigate the app.

			drawerToggle = new ActionBarNavigationToggle(
				this,                       // The host of the action bar.
				drawerLayout,               // The drawer layout.
				Resource.String.openDrawer, // The string that represents the drawer opening.
				Resource.String.closeDrawer // The string that represents the drawer closing.
			);

			SetSupportActionBar( toolbar ); // Makes the toolbar the action bar.

			drawerLayout.AddDrawerListener( drawerToggle ); // Adds the drawer toggle to the drawer layout.

			SupportActionBar.SetDisplayHomeAsUpEnabled( true ); // Set's up the proper icon and enables it.
			SupportActionBar.SetDisplayShowTitleEnabled( true ); // Set's up the title.

			drawerToggle.SyncState(); // Syncs the state.

			navDrawer.SetNavigationItemSelectedListener( this ); // Set's the navigation item selection listener to this object.

			ConnectedToWifi();
		}

		private void ConnectedToWifi() {
			AlertDialog.Builder builder; // Holds a reference to an alert builder.
			ConnectivityManager connectivity = ( ConnectivityManager )GetSystemService( ConnectivityService ); // Grabs the connectivity manager.


			if ( !connectivity.IsActiveNetworkMetered ) { // Checks if the user is not on a metered connection.
				LoadStartFragment();
				return; // Returns.
			}

			builder = new AlertDialog.Builder( this, Resource.Style.e621BaseTheme ); // Creates a new builder.

			builder.SetMessage( Resource.String.meteredConnectionDetectedMessage ); // Set's the message for the alert dialog.
			builder.SetTitle( Resource.String.meteredConnectionDetected ); // Set's the title of the alert dialog.
			builder.SetPositiveButton( Resource.String.meteredConnectionDetectedPositive, YesClicked ); // Set's the positive button's text and method.
			builder.SetNegativeButton( Resource.String.meteredConnectionDetectedNegitive, NoClicked ); // Set's the negitive button's text and method.
			builder.SetNeutralButton( Resource.String.meteredConnectionDetectedNeutral, Roulette ); // Set's the neutral button's text and method.
			builder.SetCancelable( false );

			meteredDetectedAlert = builder.Show(); // Shows the dialog.
		}

		private void LoadStartFragment() {
			FragmentTransaction trans = SupportFragmentManager.BeginTransaction();

			trans.Add( Resource.Id.frame_layout, new Posts(), "Posts" );
			trans.Commit();
		}

		private void Roulette( object sender, DialogClickEventArgs e ) {
			StartActivity( typeof( Roulette ) );
		}

		private void NoClicked( object sender, DialogClickEventArgs e ) {
			if ( sender is AlertDialog ) { // Checks to see if the alert dialog sent it.
				Finish(); // Closes the app. 
			}
		}

		private void YesClicked( object sender, DialogClickEventArgs e ) {
			if ( sender is AlertDialog aDialog ) { // Checks to see if the alert dialog sent it and auto-cast to an alert dialog.
				aDialog.Dismiss(); // Dismisses the alert dialog.
				LoadStartFragment();
			} else {
				Log.Wtf( "e621.What", $"This motherfucker was able to call YesClicked: {sender.GetType().FullName}" ); // I do not know why this code would execute.
			}
		}
	}
}