﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;

using AndroidUri = Android.Net.Uri; // Alias for Android's Uri so that a conflict does not arise from System.Uri.

namespace e621AndroidApp.Activities {
	[Activity( Label = "Roulette" )]
	public class Roulette : AppCompatActivity {
		/// <summary>
		/// An array of songs that will be psudo-randomly picked from.
		/// </summary>
		public static readonly string[] YOUTUBE_VIDEOS = new string[]{
			"https://youtu.be/Mz3Mi_OZYno", // All the Single Furries
			"https://youtu.be/oIZNrXYOitc", // KUURO - What U Wanna Do (feat. Spencer Ludwig) [Monstercat Lyric Video]
			"https://youtu.be/OEU3t0KMhrU", // KUURO - Knockin' [Monstercat Release]
			"https://youtu.be/_7AAml9gHXU", // KUURO - Afraid of the Dark (feat. Sophiya) [Official Lyric Video]
			"https://youtu.be/Som6aJQnwE8", // Dirty Audio - Racks [Monstercat Release]
			"https://youtu.be/ROF-GRsyeuw", // KUURO - Omen [Monstercat Release]
			"https://youtu.be/_y3RvDnQonM", // KUURO - Swarm VIP [Monstercat FREE Release]
			"https://youtu.be/QiTGUW3qLpU", // KUURO - Doji [Monstercat Release]
			"https://youtu.be/6jKRrCOxfbg", // KUURO - Swarm [Monstercat Release]
			"https://youtu.be/5-Vvm1fTnI0", // KUURO - Rapture (feat. MC Mota) [Monstercat Release]
			"https://youtu.be/h-hY5T5R1dA", // RIOT - Disorder [Monstercat Release]
			"https://youtu.be/01_aV8b-B_s", // KUURO - Possession [Monstercat Release]
			"https://youtu.be/uLiCy422lkQ", // KUURO - Bandit [Monstercat Release]
			"https://youtu.be/tq4xUj5AkSc", // KUURO - Savage [Monstercat Release]
			"https://youtu.be/AeYwecNzcSo", // Aero Chord - Resistance [Monstercat Release]
			"https://youtu.be/3utMV9ITxsE", // KUURO - Aamon [Monstercat Release]
			"https://youtu.be/J_01PfSjMi8", // KUURO & Clockvice - 1000 Cuts [Monstercat Release]
			"https://youtu.be/uRilGdo9fcQ", // Slippy - Promise Me [Monstercat Release]
		};

		protected override void OnCreate( Bundle savedInstanceState ) {
			Button rouletteButton;

			base.OnCreate( savedInstanceState );

			SetContentView( Resource.Layout.roulette ); // Set's the content view to the roulette view.

			rouletteButton = FindViewById<Button>( Resource.Id.roulette_button ); // Grabs the button.
			rouletteButton.Click += ( object sender, EventArgs e ) => { // Adds a virtual method to the delegate.
				int video;
				Intent youtubeIntent;
				Random ran = new Random(); // Creates a new random class, for some reason I have to do this.

				video = ran.Next( YOUTUBE_VIDEOS.Length ); // Grabs a psudo-random value between 0 and the length of the array.

				youtubeIntent = new Intent( Intent.ActionView, AndroidUri.Parse( YOUTUBE_VIDEOS[video] ) ); // Creates a new intent that links to youtube.

				StartActivity( youtubeIntent ); // Starts the intent.
			};
		}
	}
}