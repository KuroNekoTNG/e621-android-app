﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

using Newtonsoft.Json.Linq;

namespace e621AndroidApp.E621 {

	/// <summary>
	/// Used to denote the posts status.
	/// </summary>
	public enum Status {
		/// <summary>
		/// Active means that the post has been approved by the moderator.
		/// </summary>
		Active = 0,
		/// <summary>
		/// Flagged means that a moderator has flagged the post for a reason.
		/// </summary>
		Flagged = -2,
		/// <summary>
		/// Pending means that a moderator has not yet reviewed the post.
		/// </summary>
		Pending = 1,
		/// <summary>
		/// Deleted means that the post has been deleted.
		/// </summary>
		Deleted = -1
	}

	/// <summary>
	/// Used to denote the rating of the post.
	/// </summary>
	public enum Rating {
		/// <summary>
		/// Means this post is safe.
		/// </summary>
		Safe = 's',
		/// <summary>
		/// Means this post is questionable.
		/// </summary>
		Questionable = 'q',
		/// <summary>
		/// Means this post is not safe for work.
		/// </summary>
		Explicit = 'e'
	}

	/// <summary>
	/// Holds data related to a post.
	/// </summary>
	public struct Post : IComparable<Post> {

		/// <summary>
		/// The ID of the post.
		/// </summary>
		public uint PostID {
			get;
		}

		/// <summary>
		/// The ID of the person who post it.
		/// </summary>
		public uint AuthorID {
			get;
		}

		/// <summary>
		/// The size of the content.
		/// </summary>
		public uint FileSize {
			get;
		}

		/// <summary>
		/// The width of the content.
		/// </summary>
		public uint Width {
			get;
		}

		/// <summary>
		/// The height of the content.
		/// </summary>
		public uint Height {
			get;
		}

		/// <summary>
		/// The score of the post.
		/// </summary>
		public int Score {
			get;
		}

		/// <summary>
		/// The description of the post.
		/// </summary>
		public string Description {
			get;
		}

		/// <summary>
		/// The MD5 checksum of the image.
		/// </summary>
		public string MD5_Checksum {
			get;
		}

		/// <summary>
		/// The Universal Resource Locator of the file on the server.
		/// </summary>
		public string FileURL {
			get;
		}

		/// <summary>
		/// The extension of the file.
		/// </summary>
		public string FileExtension {
			get;
		}

		/// <summary>
		/// The preview link to the file.
		/// </summary>
		public string PreviewFileURL {
			get;
		}

		/// <summary>
		/// The name of the file.
		/// </summary>
		public string FileName {
			get;
		}

		/// <summary>
		/// The Universal Resource Locator of the post.
		/// </summary>
		public string PostURL {
			get;
		}

		/// <summary>
		/// The rating of the post.
		/// </summary>
		public Rating Rating {
			get;
		}

		/// <summary>
		/// The status of the post.
		/// </summary>
		public Status Status {
			get;
		}

		/// <summary>
		/// A read only collection of tags.
		/// </summary>
		public ReadOnlyCollection<string> Tags {
			get;
		}

		/// <summary>
		/// A read only collection of artists.
		/// </summary>
		public ReadOnlyCollection<string> Artists {
			get;
		}

		/// <summary>
		/// This constructor converts a dynamic data into the proper struct.
		/// </summary>
		/// <param name="post">The dynamic data.</param>
		public Post( dynamic post ) {
			PostID = ( uint )post.id; // Grabs and castes the post id.
			AuthorID = ( uint )post.creator_id; // Grabs and castes the creator id.
			Width = ( uint )post.width; // Grabs and castes the post width.
			Height = ( uint )post.height; // Grabs and castes the post height.
			Score = ( int )post.score; // Grabs and castes the post score.
			Tags = new ReadOnlyCollection<string>( ( ( string )post.tags ).Split( ' ' ) ); // Creates a new readonly with the tags.
			Artists = new ReadOnlyCollection<string>( ( ( JArray )post.artist ).ToObject<string[]>() ); // Creates a new readonly with the artists.
			Status = ( Status )Enum.Parse( typeof( Status ), ( string )post.status, true ); // Grabs the status of the post.
			Description = ( string )post.discription; // Grabs and castes the post description.
			FileURL = ( string )post.file_url; // Grabs and castes the file URL.
			PreviewFileURL = ( string )post.preview_url; // Grabs and castes the preview image URL.
			FileName = FileURL.Substring( FileURL.LastIndexOf( '/' ) + 1 ); // Grabs the name of the file.
			PostURL = $"https://e621.net/post/show/{PostID}"; // Creates the URL to the post.

			if ( Status != Status.Deleted ) { // Checks to see if the post is deleted.
				FileSize = ( uint )post.file_size; // Grabs and castes the file size.
				MD5_Checksum = ( string )post.md5; // Grabs and castes the post MD5 checksum.
				FileExtension = ( string )post.file_ext; // Grabs and castes the file extension.
			} else { // Just set's the values to their default.
				FileSize = default;
				MD5_Checksum = default;
				FileExtension = default;
			}

			switch ( ( char )post.rating ) { // Assigns a rating.
				case 's':
				case 'S':
					Rating = Rating.Safe; // Assings a rating of safe.
					break;
				case 'q':
				case 'Q':
					Rating = Rating.Questionable; // Assigns a rating of questionable.
					break;
				case 'e':
				case 'E':
				default:
					Rating = Rating.Explicit; // Assigns a rating of explicit if their is none or is marked that.
					break;
			}
		}

		/// <summary>
		/// Runs a check to make sure the image matches the checksum.
		/// </summary>
		/// <returns>True if the image matches the checksum.</returns>
		public bool DoesImageMatchChecksum() {
			MD5 copy = MD5.Create(); // Creates a new MD5 checksum generator.
			byte[] retrived;

			using ( MemoryStream image = GetMainFileStream() ) { // Grabs a new MemoryStream for computing.
				retrived = copy.ComputeHash( image ); // Generates the checksum.
			}

			return MD5_Checksum == BitConverter.ToString( retrived ).Replace( "-", "" ).ToLowerInvariant(); // Returns if the checksum matches.
		}

		/// <summary>
		/// Grabs the main file.
		/// </summary>
		/// <returns>A byte[] represing the file.</returns>
		public byte[] GetFileData() {
			byte[] data;

			using ( WebClient client = new WebClient() ) { // Gets a new WebClient for use.
				client.Headers.Add( HttpRequestHeader.UserAgent, "Kuroneko's Unoffical e621 Android App" ); // Creates that the UserAgent is Kuroneko's Unoffical e621 Android App.

				data = client.DownloadData( FileURL ); // Grabs the data from the server.
			}

			return data; // Returns the data.
		}

		/// <summary>
		/// Grabs the a preview of that data.
		/// </summary>
		/// <returns>A byte[] representing the file.</returns>
		public byte[] GetPreviewImageData() {
			byte[] data;

			using ( WebClient client = new WebClient() ) { // Gets a new WebClient for use.
				client.Headers.Add( HttpRequestHeader.UserAgent, "Kuroneko's Unoffical e621 Android App" ); // Adds a new header indicating that it is Kuroneko's Unoffical e621 Android App.

				data = client.DownloadData( PreviewFileURL ); // Grabs the file from the server.
			}

			return data; // Returns the data.
		}

		/// <summary>
		/// Gets a memory stream that will act as a wrapper for the main file.
		/// </summary>
		/// <returns>A MemoryStream that is wrapped around the data.</returns>
		public MemoryStream GetMainFileStream() {
			byte[] data = GetFileData(); // Grabs the data of the file.

			return new MemoryStream( data, 0, data.Length, false, false ); // Returns a new MemoryStream.
		}

		/// <summary>
		/// Gets a memory stream that will act as a wrapper for the preview file.
		/// </summary>
		/// <returns>A MemoryStream that is wrapped around the data.</returns>
		public MemoryStream GetPreviewImageStream() {
			byte[] data = GetPreviewImageData(); // Grabs the data of the file.

			return new MemoryStream( data, 0, data.Length, false, false ); // Returns a new MemoryStream.
		}

		/// <summary>
		/// Gets a special formatted version of the size.
		/// </summary>
		/// <returns>A string representing the size.</returns>
		public string GetFileSize() {
			byte scale = 0; // Used to denote what postfix to use.
			double size = FileSize; // The size of the image.
			StringBuilder builder = new StringBuilder(); // A string builder for building the string.

			while ( 1023f < size ) { // Loops until the size is less than the next step.
				size /= 1024f; // Bumps the size up to the next size.
				++scale; // Increases value.
			}

			builder.AppendFormat( "{0:0.00} ", size ); // Appeds the formatted string.

			switch ( scale ) { // Checks to see what scale it is at.
				case 0:
					builder.Append( "B" ); // Appends the Bytes symbol.
					break;
				case 1:
					builder.Append( "KiB" ); // Appends the Kibibyte symbol.
					break;
				case 2:
					builder.Append( "MiB" ); // Appends the Mebibyte symbol.
					break;
				case 3:
					builder.Append( "GiB" ); // Appends the Gibibyte symbol.
					break;
				default:
					throw new Exception( "Why is an image this big?" ); // Throws an exception saying that something is off.
			}

			return builder.ToString(); // Returns the string.
		}

		public int CompareTo( Post other ) {
			return PostID.CompareTo( other.PostID );
		}
	}
}